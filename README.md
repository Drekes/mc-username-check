# mc-username-check

This is a small python web scaper to check if a username in minecraft is available or not.

Uses https://namemc.com to query availability.

##### Requires BeautifulSoup & requests.
```
pip3 install --user beautifulsoup4 requests
```
