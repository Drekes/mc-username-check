#!/usr/bin/env python3
#
# mc-username-check.py
#
# This is a small web scarpee to check the availability of
# a minecraft username.
# Uses http://namemc.com to query names.
#
# Return 0 when unavailable, 1 when available and -1 on error.
import sys
import requests
from bs4 import BeautifulSoup

query_url = 'https://namemc.com/search?q='


def prRed(skk): print("\033[91m {}\033[00m" .format(skk))


def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))


def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))


def check_username(name):
    global query_url
    r = requests.get(query_url + name)

    if r.status_code == 429:
        prRed('[*] Error 429: Too Many Requests')
        prRed('[*] Please wait a few minutes before trying again...')
        exit(-1)

    elif r.status_code != 200:
        prRed('[*] An error has occured. Server responded with status: ' + str(r.status_code))
        exit(-1)

    soup = BeautifulSoup(r.content, 'html.parser')
    div_with_status = soup.find('div', 'col-sm-6 my-1')
    available = div_with_status.get_text().split('\n')[2]

    if available == 'Available*':
        return 0
    elif available == 'Unavailable':
        return 1
    else:
        return -1


def usage():
    print('Usage: ' + sys.argv[0] + ' [options] USERNAME')
    print('Check if USERNAME is available or not.')
    print('\tOptions:')
    print('\t-h/--help\t: Display this help\n')
    print('Returns 0 when unavailable, 1 when available & -1 on error.')


def main():
    if len(sys.argv) != 2 or sys.argv[1] == '--help' or sys.argv[1] == '-h':
        usage()
        exit(-1)

    prYellow('[*] Checking availability of username: ' + sys.argv[1])

    answer = check_username(sys.argv[1])

    if answer == -1:
        prRed('[*] An error has occurred. Please try again later.')
    elif answer == 1:
        prRed('[-] ' + sys.argv[1] + ' is already taken.')
    else:
        prGreen('[+] ' + sys.argv[1] + ' is still available!')

    return answer


if __name__ == '__main__':
    main()
